#!/usr/bin/make -f

# Keep this list of folders that are not included in the
# orig syncronized with the one in debian/source/local-options!
VERSIONFULL := $(shell dpkg-parsechangelog | grep -x "Version:.*" | sed 's@Version: \(.\+\)-.\+@\1@')
VERSION := $(shell echo $(VERSIONFULL) | sed 's@\(.\+\)+.\+@\1@')
ORIG_CONTENT := $(shell git ls-tree --name-only v$(VERSION) | grep -Ev '^(third_party|debian|\.git.*)')

# Don't enable pie, otherwise the unit tests don't work.
export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

# Append CPPFLAGS, because cmake doesn't know that.
CFLAGS += $(CPPFLAGS)
CXXFLAGS += $(CPPFLAGS)

# Use this variable to allow options passed to cmake to be overridable
DEB_CMAKE_OPTIONS ?= -DCMAKE_VERBOSE_MAKEFILE=ON \
                     -DCMAKE_INSTALL_PREFIX="/usr" \
		     -DCMAKE_C_FLAGS="$(CFLAGS)" \
		     -DCMAKE_CXX_FLAGS="$(CXXFLAGS)"

DEB_ASE_CMAKE_OPTIONS ?= -DENABLE_UPDATER:BOOL=OFF \
                         -DENABLE_TESTS:BOOL=ON \
                         -DUSE_SHARED_CURL:BOOL=ON \
                         -DUSE_SHARED_GIFLIB:BOOL=ON \
                         -DUSE_SHARED_JPEGLIB:BOOL=ON \
                         -DUSE_SHARED_LIBPNG:BOOL=ON \
                         -DUSE_SHARED_ZLIB:BOOL=ON \
                         -DUSE_SHARED_TINYXML:BOOL=ON \
                         -DUSE_SHARED_GTEST:BOOL=ON \
                         -DUSE_SHARED_ALLEGRO4:BOOL=ON \
                         -DUSE_SHARED_LIBLOADPNG:BOOL=ON \
                         -DUSE_SHARED_PIXMAN:BOOL=ON \
                         -DUSE_SHARED_FREETYPE:BOOL=ON \
                         -DUSE_SHARED_MODP_B64:BOOL=ON

%:
	dh $@ --parallel

override_dh_auto_clean:
	rm -rf build

override_dh_auto_configure:
	cd third_party && find ! \( -name 'simpleini' -o -name 'duktape' -o -name '.' \) -type d -exec rm -rf {} +
	rm -rf src/allegro
	rm -rf data/www
	mkdir build && cd build && cmake $(DEB_CMAKE_OPTIONS) $(DEB_ASE_CMAKE_OPTIONS) ..
	mkdir build/gtest && cd build/gtest && cmake $(DEB_CMAKE_OPTIONS) /usr/src/gtest

override_dh_auto_build:
	dh_auto_build -Dbuild/gtest
	dh_auto_build -Dbuild

override_dh_auto_install:
	dh_auto_install -Dbuild
	install -D -m 644 data/icons/ase16.png debian/aseprite/usr/share/icons/hicolor/16x16/apps/aseprite.png
	install -D -m 644 data/icons/ase32.png debian/aseprite/usr/share/icons/hicolor/32x32/apps/aseprite.png
	install -D -m 644 data/icons/ase48.png debian/aseprite/usr/share/icons/hicolor/48x48/apps/aseprite.png
	install -D -m 644 data/icons/ase64.png debian/aseprite/usr/share/icons/hicolor/64x64/apps/aseprite.png

override_dh_installchangelogs:
	dh_installchangelogs -k debian/upstream-changelog

get-orig-source:
	git archive --prefix=aseprite_$(VERSION)/ --format=tar v$(VERSION) $(ORIG_CONTENT) third_party/CMakeLists.txt > aseprite.tar
	git submodule update --init third_party/simpleini/
	cd third_party/simpleini && git archive  --prefix="aseprite_$(VERSION)/third_party/simpleini/" --format=tar HEAD > ../../simpleini.tar
	git submodule update --init third_party/duktape/
	cd third_party/duktape && git archive  --prefix="aseprite_$(VERSION)/third_party/duktape/" --format=tar HEAD > ../../duktape.tar
	git submodule update --init src/flic/
	cd src/flic && git archive  --prefix="aseprite_$(VERSION)/src/flic/" --format=tar HEAD > ../../flic.tar
	git submodule update --init src/clip/
	cd src/clip && git archive  --prefix="aseprite_$(VERSION)/src/clip/" --format=tar HEAD > ../../clip.tar
	git submodule update --init src/observable/
	cd src/observable && git archive  --prefix="aseprite_$(VERSION)/src/observable/" --format=tar HEAD > ../../observable.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar aseprite.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar simpleini.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar duktape.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar flic.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar clip.tar
	tar -Af ../aseprite_$(VERSIONFULL).orig.tar observable.tar
	gzip ../aseprite_$(VERSIONFULL).orig.tar
	rm aseprite.tar simpleini.tar duktape.tar flic.tar clip.tar observable.tar
